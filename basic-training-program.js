let rutinesXDia = [[15, 14, 14, 13, 11], [15, 14, 14, 9, 10], [20, 14, 14, 13, 11]];
console.log(getMinReps(rutinesXDia));

function getMinReps(rutinesXDia) {
    let newArray = []
    for (let i = 0; i < rutinesXDia.length; i++) {
        min = Math.min(...rutinesXDia[i]);
        newArray.push(min);
    }
    return newArray;
}