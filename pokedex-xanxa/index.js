
function listPokemons(){
    fetch('https://pokeapi.co/api/v2/pokemon')
    .then(response => response.json())
    .then(function(allpokemon){

        let bodyTable = document.getElementById("listPokemon");
        allpokemon.results.forEach(function(pokemon){
            let tmpRow='<tr><td class="right">'+pokemon.name+'</td><td class="left"><button type="button" data-url="' + pokemon.url +'" onclick="getDetailPokemon(this);">Ver Detalles</button></td></tr>';
            let row = document.createElement("tr");
            row.innerHTML=tmpRow;
            bodyTable.appendChild(row);
        })
    })
}

function getDetailPokemon(e){
    fetch(e.dataset.url)
    .then(response => response.json())
    .then(function(detailPokemon){
        document.getElementById("photo").src = detailPokemon.sprites.back_default;
        document.getElementById("name").textContent="Nombre: "+detailPokemon.name;
        document.getElementById("weight").textContent="Peso: "+detailPokemon.weight;
        document.getElementById("height").textContent="Altura: "+detailPokemon.height;
    })
}


